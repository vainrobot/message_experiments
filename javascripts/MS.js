$("#addABackgroundColorToAChildIframeButton").on("click", function(e){
	MS.addABackgroundColorToAChildIframe($("#child_frame")[0], "rgba(0,255,0,0.5)");
});

$("#addABackgroundColorToAChildIframeCrossDomainButton").on("click", function(e){
	MS.addABackgroundColorToAChildIframeCrossDomain($("#cross_domain_child_frame")[0], "rgba(0,255,0,0.5)");
});

$("#sayHelloToAChildIframeButton").on("click", function(e){
	MS.sayHelloToAChildIframe($("#child_frame_two")[0], "hello");
});


var MS = (function(){
	function _release_public(){
		return {
			init: init,
			addABackgroundColorToAChildIframe: addABackgroundColorToAChildIframe,
			addABackgroundColorToAChildIframeCrossDomain: addABackgroundColorToAChildIframeCrossDomain,
			sayHelloToAChildIframe: sayHelloToAChildIframe
		}
	};
	
	/*----------private-----------*/
	
	
	
	
	/*----------public------------*/
	
	/**
	 * Add A Background Color To A Child Iframe
	 * 
	 * @param DOMElement iframe
	 * @param String rgba_background_colorS
	 * 
	 * @return null
	 */
	function addABackgroundColorToAChildIframe(child_frame, rgba_background_colorS){
		var content = child_frame.contentDocument;
		console.log("CV::AddABackgroundColorToAChildIframe: With a color of: " + rgba_background_colorS);
		content.body.style.backgroundColor = rgba_background_colorS;
		return null;
	};
	
	/**
	 * Add A Background Color To A Child Iframe Cross Domain
	 * 
	 * @param DOMElement iframe (Cross domain)
	 * @param String rgba_background_colorS
	 * 
	 * @return null
	 */
	function addABackgroundColorToAChildIframeCrossDomain(cross_domain_child_frame, rgba_background_colorS){
		var content = cross_domain_child_frame.contentDocument;
		console.log("CV::AddABackgroundColorToAChildIframeCrossDomain: With a color of: " + rgba_background_colorS);
		console.log("this should fail silently");
		content.body.style.backgroundColor = rgba_background_colorS;
		return null;
	};
	
	/**
	 * Say Hello To A Child Iframe
	 * 
	 * @param DOMElement iframe
	 * @param String hello
	 * 
	 * @return null
	 */
	function sayHelloToAChildIframe(child_frame_two, saying_helloS){
		var content = child_frame_two.contentDocument,
			my_json = {
				name: "Rick",
				age: "far too old",
				message_text: saying_helloS
			};
		console.log("CV::SayHelloToAChildIframe: Saying hello to the child with this phrase: " + saying_helloS);
	
	
		console.log("The name of this frame I wish to send stuff to is: " + child_frame_two.name);
		//as a shortcut I named this frame child_message already in the HTML
		
		
		child_message.postMessage(my_json, "http://localhost:8000");
		
		content.body.style.backgroundColor = "pink";
		
		//we could reach in voa the DOM but that defeats the point of showing Cross domain vs same domain
		
		
		return null;
	};
	
	
	function init(){
		//console.log("MS::init: Good to go!");	
	};
		
	return _release_public();
})();



	


