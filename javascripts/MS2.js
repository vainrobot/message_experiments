var MS2 = (function(){
	function _release_public(){
		return {
			init: init
		}
	};
	
	
	/*----------private-------------*/
	
	/**
	 * Got Tweets 
	 * just display them basically as links using webintents for more fun.
	 */
	function _gotTweets(e){
		var html = '',
			i=0;
		for(;i<e.length;i+=1){
			if(e[i].user.id_str && e[i].text && e[i].user.profile_image_url && e[i].user.name){
				html += '<a href="https://twitter.com/intent/retweet?tweet_id=' + e[i].id_str + '"><li id="' + e[i].user.id_str + '"><img width="40" height="40" src="' + e[i].user.profile_image_url + '"></img><span class="handle"> ' + e[i].user.name + ' </span><span class="handle_body">' + e[i].text  + '</span>' + '</li></a>';
			}
		}
		if(html){
			$("#getting_tweets").hide();
			$("#twitter_posts").prepend(html);
		}
	}
	
	/**
	 * Get Tweets
	 * Get X public tweets from a persons account
	 * @param {String} person
	 * @param {Integer} how_much_can_you_takeI (X)
	 */
	function _get_tweets(person, how_much_can_you_takeI){
		console.info("Ok hitting up twitter and getting " + how_much_can_you_takeI + " of " + person + "'s tweets");
		var url = "https://api.twitter.com/1/statuses/user_timeline.json?callback=?";
			data = {
				screen_name: person,
				count: how_much_can_you_takeI,
				include_rts: false,
				include_entities: false
			};
		$.getJSON(url, data, _gotTweets);
	};
	
	
	

	
	/*----------public--------------*/
	
	function init(){
		console.log("MS2::init: Good to go!");
		_get_tweets("MarkoGargenta", 5);
		twttr.ready(function(twttr){
			console.info("Twitter web intents are ready");
			//we could capture events here such as retweet and track them with our own analytics
		});
	};
	
	

	
	return _release_public();
})();



	


