Post Message API experiments
============================

	Show that a parent can alter the DOM of the child Iframe if the iframe is of the same origin (localhost) √
	Show that this does not work across domain boundaries √
	Show that I can call a iframe on the server and send it a message and it display it √
	Show that you can get info from twitter (client side) √ << index3.html
	
It is best if you use localhost:8000 for running this

	python -m SimpleHTTPServer 8000
	
and of course open firebug console
	
Hey I found something interesting:
----------------------------------

When I load an external site twice i.e in iframes 2a nd 3, the first one fails in firefox and the second one works.
I took this out btw as it screwed with stuff. But you can probably recreate it.

Also note how you need to be running the server on localhost for postMessage to work if you just run from file it (number3) fails.